from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# Create your views here.
@login_required
def receipts_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipt,
    }
    return render(request, "receipts/list.html", context)

def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect ("home")

    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect ("category_list")

    else:
        form = CategoryForm()
        context = {
            "form": form,
        }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect ("account_list")

    else:
        form = AccountForm()
        context = {
            "form": form,
        }
    return render(request, "receipts/create_account.html", context)

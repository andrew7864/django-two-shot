from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Create your models here.
@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    models_display = [
            "name",
            "owner",
        ]

@admin.register(Account)
class Account(admin.ModelAdmin):
    models_display = [
            "name",
            "number",
            "owner"
        ]


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    models_display = [
            "vendor",
            "total",
            "tax",
            "date",
            "purchaser",
            "category",
            "account"
        ]
